<?php

add_action('init', 'aen_plugin_posttype');
/**
 * Add custom post type for companies
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function aen_plugin_posttype() {
    $labels = array(
        'name' => _x('Plugin', 'post type general name', 'aen'),
        'singular_name' => _x('Plugin', 'post type singular name', 'aen'),
        'menu_name' => _x('Plugin', 'admin menu', 'aen'),
        'name_admin_bar' => _x('Plugin', 'add new on admin bar', 'aen'),
        'add_new' => _x('Add new', 'Plugin', 'aen'),
        'add_new_item' => __('Add new Plugin', 'aen'),
        'new_item' => __('New Plugin', 'aen'),
        'edit_item' => __('Edit Plugin', 'aen'),
        'view_item' => __('View Plugin', 'aen'),
        'all_items' => __('All Plugins', 'aen'),
        'search_items' => __('Search Plugin', 'aen'),
        'not_found' => __('Plugin not found', 'aen'),
        'not_found_in_trash' => __('Plugin not found in trash', 'aen')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('All Plugins', 'aen'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'plugin'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-admin-settings',
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
    );

    register_post_type('Plugin', $args);
}
