<?php

add_action('init', 'aen_tama_posttype');
/**
 * Add custom post type for companies
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function aen_tama_posttype() {
    $labels = array(
        'name' => _x('Tama', 'post type general name', 'aen'),
        'singular_name' => _x('Tama', 'post type singular name', 'aen'),
        'menu_name' => _x('Tama', 'admin menu', 'aen'),
        'name_admin_bar' => _x('Tama', 'add new on admin bar', 'aen'),
        'add_new' => _x('Add new', 'lab-team', 'aen'),
        'add_new_item' => __('Add new Tama', 'aen'),
        'new_item' => __('New Tama', 'aen'),
        'edit_item' => __('Edit Tama', 'aen'),
        'view_item' => __('View Tama', 'aen'),
        'all_items' => __('All Tamas', 'aen'),
        'search_items' => __('Search Tama', 'aen'),
        'not_found' => __('Tama not found', 'aen'),
        'not_found_in_trash' => __('Tama not found in trash', 'aen')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('All Tamas', 'aen'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'tama'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-awards',
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
    );

    register_post_type('Tama', $args);
}
