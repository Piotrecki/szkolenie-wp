<?php

add_action('init', 'aen_Food_posttype');
/**
 * Add custom post type for companies
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function aen_Food_posttype() {
    $labels = array(
        'name' => _x('Food', 'post type general name', 'aen'),
        'singular_name' => _x('Food', 'post type singular name', 'aen'),
        'menu_name' => _x('Food', 'admin menu', 'aen'),
        'name_admin_bar' => _x('Food', 'add new on admin bar', 'aen'),
        'add_new' => _x('Add new', 'lab-team', 'aen'),
        'add_new_item' => __('Add new Food', 'aen'),
        'new_item' => __('New Food', 'aen'),
        'edit_item' => __('Edit Food', 'aen'),
        'view_item' => __('View Food', 'aen'),
        'all_items' => __('All Foods', 'aen'),
        'search_items' => __('Search Food', 'aen'),
        'not_found' => __('Food not found', 'aen'),
        'not_found_in_trash' => __('Food not found in trash', 'aen')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('All Foods', 'aen'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'Food'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-carrot',
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
    );

    register_post_type('Food', $args);
}
