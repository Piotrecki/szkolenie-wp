<?php

add_action('init', 'aen_others_posttype');
/**
 * Add custom post type for companies
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function aen_others_posttype() {
    $labels = array(
        'name' => _x('Others', 'post type general name', 'aen'),
        'singular_name' => _x('Others', 'post type singular name', 'aen'),
        'menu_name' => _x('Others', 'admin menu', 'aen'),
        'name_admin_bar' => _x('Others', 'add new on admin bar', 'aen'),
        'add_new' => _x('Add new', 'Others', 'aen'),
        'add_new_item' => __('Add new Others', 'aen'),
        'new_item' => __('New Others', 'aen'),
        'edit_item' => __('Edit Others', 'aen'),
        'view_item' => __('View Others', 'aen'),
        'all_items' => __('All Otherss', 'aen'),
        'search_items' => __('Search Others', 'aen'),
        'not_found' => __('Others not found', 'aen'),
        'not_found_in_trash' => __('Others not found in trash', 'aen')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('All Otherss', 'aen'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'others'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-paperclip',
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
    );

    register_post_type('Others', $args);
}
