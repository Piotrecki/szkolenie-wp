<?php

add_action('init', 'aen_sensual_posttype');
/**
 * Add custom post type for companies
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function aen_sensual_posttype() {
    $labels = array(
        'name' => _x('Sensual', 'post type general name', 'aen'),
        'singular_name' => _x('Sensual', 'post type singular name', 'aen'),
        'menu_name' => _x('Sensual', 'admin menu', 'aen'),
        'name_admin_bar' => _x('Sensual', 'add new on admin bar', 'aen'),
        'add_new' => _x('Add new', 'lab-team', 'aen'),
        'add_new_item' => __('Add new Sensual', 'aen'),
        'new_item' => __('New Sensual', 'aen'),
        'edit_item' => __('Edit Sensual', 'aen'),
        'view_item' => __('View Sensual', 'aen'),
        'all_items' => __('All Sensuals', 'aen'),
        'search_items' => __('Search Sensual', 'aen'),
        'not_found' => __('Sensual not found', 'aen'),
        'not_found_in_trash' => __('Sensual not found in trash', 'aen')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('All Sensuals', 'aen'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'sensual'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-heart',
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
    );

    register_post_type('Sensual', $args);
}
