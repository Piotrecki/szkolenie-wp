<?php

add_action('init', 'aen_clubs_posttype');
/**
 * Add custom post type for companies
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function aen_clubs_posttype() {
    $labels = array(
        'name' => _x('Club', 'post type general name', 'aen'),
        'singular_name' => _x('Club', 'post type singular name', 'aen'),
        'menu_name' => _x('Club', 'admin menu', 'aen'),
        'name_admin_bar' => _x('Club', 'add new on admin bar', 'aen'),
        'add_new' => _x('Add new', 'lab-team', 'aen'),
        'add_new_item' => __('Add new Club', 'aen'),
        'new_item' => __('New Club', 'aen'),
        'edit_item' => __('Edit Club', 'aen'),
        'view_item' => __('View Club', 'aen'),
        'all_items' => __('All Clubs', 'aen'),
        'search_items' => __('Search Club', 'aen'),
        'not_found' => __('Club not found', 'aen'),
        'not_found_in_trash' => __('Club not found in trash', 'aen')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('All Clubs', 'aen'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'clubs'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-format-audio',
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
    );

    register_post_type('Club', $args);
}
