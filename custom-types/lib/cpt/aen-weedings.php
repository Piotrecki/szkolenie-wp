<?php

add_action('init', 'aen_weedings_posttype');
/**
 * Add custom post type for companies
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function aen_weedings_posttype() {
    $labels = array(
        'name' => _x('Weedings', 'post type general name', 'aen'),
        'singular_name' => _x('Weedings', 'post type singular name', 'aen'),
        'menu_name' => _x('Weedings', 'admin menu', 'aen'),
        'name_admin_bar' => _x('Weedings', 'add new on admin bar', 'aen'),
        'add_new' => _x('Add new', 'lab-team', 'aen'),
        'add_new_item' => __('Add new Weedings', 'aen'),
        'new_item' => __('New Weedings', 'aen'),
        'edit_item' => __('Edit Weedings', 'aen'),
        'view_item' => __('View Weedings', 'aen'),
        'all_items' => __('All Weedingss', 'aen'),
        'search_items' => __('Search Weedings', 'aen'),
        'not_found' => __('Weedings not found', 'aen'),
        'not_found_in_trash' => __('Weedings not found in trash', 'aen')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('All Weedingss', 'aen'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'weedings'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-businessman',
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
    );

    register_post_type('Weedings', $args);
}
