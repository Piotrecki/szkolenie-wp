<?php

add_action('init', 'aen_events_posttype');
/**
 * Add custom post type for companies
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function aen_events_posttype() {
    $labels = array(
        'name' => _x('Event ', 'post type general name', 'aen'),
        'singular_name' => _x('Event', 'post type singular name', 'aen'),
        'menu_name' => _x('Event', 'admin menu', 'aen'),
        'name_admin_bar' => _x('Event', 'add new on admin bar', 'aen'),
        'add_new' => _x('Add new', 'lab-team', 'aen'),
        'add_new_item' => __('Add new Event', 'aen'),
        'new_item' => __('New Event', 'aen'),
        'edit_item' => __('Edit Event', 'aen'),
        'view_item' => __('View Event', 'aen'),
        'all_items' => __('All Events', 'aen'),
        'search_items' => __('Search Event', 'aen'),
        'not_found' => __('Event not found', 'aen'),
        'not_found_in_trash' => __('Event not found in trash', 'aen')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('All Events', 'aen'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'events'),
        'capability_type' => 'post',
        'has_archive' => false,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-media-spreadsheet',
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
    );

    register_post_type('Event', $args);
}
